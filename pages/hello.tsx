import React from 'react'

type Props = {}

export default function Hello({}: Props) {
  return (
    <>
        <div>Hello world</div>
        <h1>My name is dreyfuss</h1>
    </>
  )
}